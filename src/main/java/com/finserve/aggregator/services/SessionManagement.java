/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.services;

import com.finserve.aggregator.pojos.SessionData;
import java.util.Hashtable;
import java.util.Map;

/**
 *
 * @author Wambayi
 */
public class SessionManagement {
    Map<String,SessionData> sessionParams = null;

    public SessionManagement() {
        this.sessionParams = new Hashtable();
    }
    
    public void newSession(String msisdn,SessionData sessionData){
        this.sessionParams.put(msisdn, sessionData);
    } 
    
    public void updateSession(String msisdn,SessionData sessionData){
        this.sessionParams.replace(msisdn, sessionData);
    }
    
    public SessionData getSessionData(String msisdn){
        SessionData sessionData = this.sessionParams.get(msisdn);
        return sessionData;        
    }
    
    public void deleteSession(String msisdn){
        this.sessionParams.remove(msisdn);
    }

    @Override
    public String toString() {
        return "SessionManagement{" + "sessionParams=" + sessionParams + '}';
    }
    
    
}
