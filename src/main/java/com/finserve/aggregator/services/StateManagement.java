/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.services;

import com.finserve.aggregator.models.MombasaCountyMenu;
import com.finserve.aggregator.models.Payments;
import com.finserve.aggregator.pojos.RegistrationData;
import com.finserve.aggregator.models.RegisteredUsers;
import com.finserve.aggregator.models.RegisteredVehicles;
import com.finserve.aggregator.models.VehicleTypes;
import com.finserve.aggregator.pojos.ExistingVehiclesMenu;
import com.finserve.aggregator.pojos.PaymentRequest;
import com.finserve.aggregator.pojos.UssdRequest;
import com.finserve.aggregator.pojos.VehicleRegistations;
import com.finserve.aggregator.repositories.MombasaCountyMenuRepository;
import com.finserve.aggregator.repositories.PaymentsRepository;
import com.finserve.aggregator.utils.Tools;
import java.util.Calendar;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.finserve.aggregator.repositories.RegisteredUsersRepository;
import com.finserve.aggregator.repositories.RegisteredVehiclesRepository;
import com.finserve.aggregator.repositories.VehicleTypesRepository;
import com.finserve.aggregator.utils.HttpUtils;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import org.springframework.core.env.Environment;

/**
 *
 * @author Wambayi
 */
@Service
public class StateManagement {
    
    static Log log = LogFactory.getLog(StateManagement.class.getName());
    
    @Autowired
    Tools tools;
    
    @Autowired
    RegisteredUsersRepository registrationsRepository;
    
    @Autowired
    MombasaCountyMenuRepository mombasaCountyMenuRepository;
    
    @Autowired
    RegisteredVehiclesRepository registeredVehiclesRepository;
    
    @Autowired
    RegisteredUsersRepository registeredUsersRepository;
    
    @Autowired
    VehicleTypesRepository vehicleTypesRepository;
    
    @Autowired
    PaymentsRepository paymentsRepository;
    
    @Autowired
    FinserveApiCall apiCall;
    
    @Autowired
    Environment env;
    
    public UssdRequest processRequest(UssdRequest request,MombasaCountyMenu currentMenu){
        
        String input = tools.extractInput(request.getInput());
        
        String mainAndBackParams = "0. Main Menu";
        String mainAndCloseParams = "0. MainMenu\n99. Close";
        
        log.info("RequestParamsIncoming: "+request.toString());
        
        switch(currentMenu.getParamFunction()){
            case "VALIDATENAME":
                
                if(input != null || !input.isEmpty()){
                    RegistrationData regData = new RegistrationData();
                    regData.setFirstName(input);
                    request.setRegistrationData(regData);
                    
                    String menu = tools.getMenu(Long.parseLong(currentMenu.getChildNodeId())) + mainAndBackParams;
                    
                    MombasaCountyMenu childMenu = 
                                    mombasaCountyMenuRepository
                                            .findMombasaCountyMenuById(Long.parseLong(currentMenu.getChildNodeId()));
                    log.info("NEXT INPUT: NewOption:"+childMenu.toString());
                    request.setPreviousMenuLevel(request.getMenuLevel());
                    request.setMenuLevel(childMenu.getId().toString());
                    request.setMenuNodeType(childMenu.getParamType());
                    request.setMessage(menu);
                    request.setFlow(1);
                    
                    log.info("RequestParams:"+request.toString());
                }else{
                    String errorMsg = "Try again\n";
                    request.setMessage(errorMsg + request.getMessage());
                }
                break;
            case "VALIDATELASTNAME":
                if(input != null || !input.isEmpty()){
                    request.getRegistrationData().setLastName(input);
                    
                    String menu = tools.getMenu(Long.parseLong(currentMenu.getChildNodeId())) + mainAndBackParams;
                    
                    MombasaCountyMenu childMenu = 
                                    mombasaCountyMenuRepository
                                            .findMombasaCountyMenuById(Long.parseLong(currentMenu.getChildNodeId()));
                    log.info("NEXT INPUT: NewOption:"+childMenu.toString());
                    request.setPreviousMenuLevel(request.getMenuLevel());
                    request.setMenuLevel(childMenu.getId().toString());
                    request.setMenuNodeType(childMenu.getParamType());
                    request.setMessage(menu);
                    request.setFlow(1);
                }else{
                    String errorMsg = "Try again\n";
                    request.setMessage(errorMsg + request.getMessage());
                }
                break;
            case "VALIDATEID":
                if(input != null || !input.isEmpty()){
                    request.getRegistrationData().setIdNumber(input);
                    String menu = tools.getMenu(Long.parseLong(currentMenu.getChildNodeId())) + mainAndBackParams;
                    
                    MombasaCountyMenu childMenu = 
                                    mombasaCountyMenuRepository
                                            .findMombasaCountyMenuById(Long.parseLong(currentMenu.getChildNodeId()));
                    log.info("NEXT INPUT: NewOption:"+childMenu.toString());
                    request.setPreviousMenuLevel(request.getMenuLevel());
                    request.setMenuLevel(childMenu.getId().toString());
                    request.setMenuNodeType(childMenu.getParamType());
                    request.setMessage(menu);
                    request.setFlow(1);
                }else{
                    String errorMsg = "Try again\n";
                    request.setMessage(errorMsg + request.getMessage());
                }
                break;
            case "VALIDATENEWPIN":
                if(input != null || !input.isEmpty()){
                    request.getRegistrationData().setNewPin(input);
                    
                    String menu = tools.getMenu(Long.parseLong(currentMenu.getChildNodeId())) + mainAndBackParams;
                    
                    MombasaCountyMenu childMenu = 
                                    mombasaCountyMenuRepository
                                            .findMombasaCountyMenuById(Long.parseLong(currentMenu.getChildNodeId()));
                    log.info("NEXT INPUT: NewOption:"+childMenu.toString());
                    request.setPreviousMenuLevel(request.getMenuLevel());
                    request.setMenuLevel(childMenu.getId().toString());
                    request.setMenuNodeType(childMenu.getParamType());
                    request.setMessage(menu);
                    request.setFlow(1);
                }else{
                    String errorMsg = "Try again\n";
                    request.setMessage(errorMsg + request.getMessage());
                }
                break;
            case "VALIDATECONFIRMPIN":
                if(input != null || !input.isEmpty()){
                    //Check if PINs Match
                    if(input.equals(request.getRegistrationData().getNewPin())){
                        request.getRegistrationData().setConfirmPin(input);
                        
                        //Register user
                        RegisteredUsers register = new RegisteredUsers();
                        register.setFirstName(request.getRegistrationData().getFirstName());
                        register.setLastName(request.getRegistrationData().getLastName());
                        register.setIdNumber(request.getRegistrationData().getIdNumber());
                        register.setMsisdn(request.getMsisdn());
                        register.setRegistrationDate(Calendar.getInstance().getTime());
                        register.setSubmitted(0);
                        registrationsRepository.save(register);
                        
                        String menu = tools.getMenu(Long.parseLong(currentMenu.getChildNodeId())) + mainAndBackParams;
                    
                        MombasaCountyMenu childMenu = 
                                        mombasaCountyMenuRepository
                                                .findMombasaCountyMenuById(Long.parseLong(currentMenu.getChildNodeId()));
                        log.info("NEXT INPUT: NewOption:"+childMenu.toString());
                        request.setPreviousMenuLevel(request.getMenuLevel());
                        request.setMenuLevel(childMenu.getId().toString());
                        request.setMenuNodeType(childMenu.getParamType());
                        request.setMessage(menu);
                        request.setFlow(1);
                        
                    }else{
                        String errorMsg = "Wrong PIN entered\n";
                        request.setMessage(errorMsg + request.getMessage());
                    }
                    
                }else{
                    String errorMsg = "Try again\n";
                    request.setMessage(errorMsg + request.getMessage());
                }
                break;
            case "VEHICLETYPE":
                //Capture the vehicle option selected
                //Input is the selected vehile type
                if(input != null || !input.isEmpty()){
                    VehicleRegistations vehicleReg = new VehicleRegistations();
                    vehicleReg.setVehicleTypeID(Integer.parseInt(input));
                    request.setVehicleReg(vehicleReg);
                    
                    //Paymen
                    
                    String menu = tools.getMenu(Long.parseLong(currentMenu.getChildNodeId())) + mainAndBackParams;
                    
                    MombasaCountyMenu childMenu = 
                                        mombasaCountyMenuRepository
                                                .findMombasaCountyMenuById(Long.parseLong(currentMenu.getChildNodeId()));
                    log.info("NEXT INPUT: NewOption:"+childMenu.toString());
                    request.setPreviousMenuLevel(request.getMenuLevel());
                    request.setMenuLevel(childMenu.getId().toString());
                    request.setMenuNodeType(childMenu.getParamType());
                    request.setMessage(menu);
                    request.setFlow(1);
                    log.info("RequestParams: VehicleType:"+request.toString());
                }else{
                    String errorMsg = "Try again\n";
                    request.setMessage(errorMsg + request.getMessage());
                }
                break;
            case "VALIDATEREGNUM":
                //Capture the vehicle registration number
                //KAJ 997F : Length 7 characters
                if(input != null || !input.isEmpty()){
                    String regNum = input.replaceAll("\\s+",""); 
                    if(regNum.length() == 7){ 
                        
                        request.getVehicleReg().setVehicleRegNum(regNum);
                        
                        RegisteredVehicles regVehicle = new RegisteredVehicles();
                        regVehicle.setRegistrationDate(Calendar.getInstance().getTime());
                        regVehicle.setRegistredUsers(registeredUsersRepository.findBymsisdn(request.getMsisdn()));
                        regVehicle.setVehicleRegistrationNumber(request.getVehicleReg().getVehicleRegNum());
                        regVehicle.setVehicleTypes(vehicleTypesRepository.findByvehicleTypeId(new Long(request.getVehicleReg().getVehicleTypeID())));
                        registeredVehiclesRepository.save(regVehicle);
                        
                        VehicleTypes vehicleTypes = 
                                vehicleTypesRepository.findByvehicleTypeId(new Long(request.getVehicleReg().getVehicleTypeID()));
                        
                        PaymentRequest paymentReq = new PaymentRequest();
                        paymentReq.setProductType("PARKING");
                        paymentReq.setPaymentChannel("EAZZYPAY");
                        paymentReq.setProductId(regNum);
                        paymentReq.setAmount(vehicleTypes.getParkingFee().toString());
                        paymentReq.setMsisdn(request.getMsisdn());
                        paymentReq.setPaymentDescription("PARKING");
                        paymentReq.setTxnId(tools.generateId());
                        
                        request.setPaymentRequest(paymentReq);
                        
                        String menu = String.format(tools.getMenu(Long.parseLong(currentMenu.getChildNodeId())),
                                vehicleTypes.getParkingFee().toString(), 
                                regNum) + mainAndBackParams;
                        
                        MombasaCountyMenu childMenu = 
                                        mombasaCountyMenuRepository
                                                .findMombasaCountyMenuById(Long.parseLong(currentMenu.getChildNodeId()));
                        log.info("NEXT INPUT: NewOption:"+childMenu.toString());
                        request.setPreviousMenuLevel(request.getMenuLevel());
                        request.setMenuLevel(childMenu.getId().toString());
                        request.setMenuNodeType(childMenu.getParamType());
                        request.setMessage(menu);
                        request.setFlow(1);
                        log.info("RequestParams: Vehicle:"+request.toString());
                    }else{
                        String errorMsg = "Invalid Car Registration number.\n";
                        request.setMessage(errorMsg + request.getMessage());
                    }
                }else{
                    String errorMsg = "Try again\n";
                    request.setMessage(errorMsg + request.getMessage());
                }
                break;
            case "REGISTEREDVEHICLES":
                
                List<RegisteredVehicles> vehicles = 
                        registeredVehiclesRepository
                                .findRegisteredVehiclesByRegistredUsers(
                                        registeredUsersRepository.findBymsisdn(request.getMsisdn()));
                
                //List<ExistingVehiclesMenu> existing = new List<ExistingVehiclesMenu()>;
                ArrayList<ExistingVehiclesMenu> existing = new ArrayList<ExistingVehiclesMenu>();
                
                String menu = "";
                
                if(vehicles.size() == 0){
                    menu+="No registered vehicles\n";
                    
                    ExistingVehiclesMenu item = new ExistingVehiclesMenu();
                    
                    int newIndex = existing.size() + 1;
                    item.setMenuOptionId(newIndex);
                    item.setVehicleRegistrationNum("Register New Vehicle");
                    item.setOptionType("OPT");
                    item.setVehicleParkingAmount("18");
                    
                    existing.add(item);
                    
                    for(ExistingVehiclesMenu items: existing){
                        menu+=items.getMenuOptionId()+". "+items.getVehicleRegistrationNum()+"\n";
                    }
                    
                    request.setExisting(existing);
                    
                }else{
                    for(int x = 0; x < vehicles.size();x++){
                        ExistingVehiclesMenu vehicleItem = new ExistingVehiclesMenu();
                        
                        vehicleItem.setMenuOptionId(x+1);
                        vehicleItem.setVehicleRegistrationNum(vehicles.get(x).getVehicleRegistrationNumber());
                        vehicleItem.setOptionType("REG");
                        vehicleItem.setVehicleParkingAmount(vehicles.get(x).getVehicleTypes().getParkingFee().toString());
                        
                        existing.add(vehicleItem);   
                    }

                    //Add option
                    int newIndex = existing.size() + 1;
                    
                    ExistingVehiclesMenu item = new ExistingVehiclesMenu();
                    item.setMenuOptionId(newIndex);
                    item.setVehicleRegistrationNum("Register New Vehicle");
                    item.setOptionType("OPT");
                    item.setVehicleParkingAmount("18");
                    existing.add(item);
                    
                    request.setExisting(existing);
                    
                    for(ExistingVehiclesMenu items: existing){
                        menu+=items.getMenuOptionId()+". "+items.getVehicleRegistrationNum()+"\n";
                    }
                }
                
                MombasaCountyMenu childMenu = 
                                        mombasaCountyMenuRepository
                                                .findMombasaCountyMenuById(Long.parseLong(currentMenu.getChildNodeId()));
                log.info("NEXT INPUT: REGISTEREDVEHICLES:"+childMenu.toString());
                request.setPreviousMenuLevel(request.getMenuLevel());
                request.setMenuLevel(childMenu.getId().toString());
                request.setMenuNodeType(childMenu.getParamType());
                request.setMessage(menu);
                request.setFlow(1);
                log.info("RequestParams: Vehicle:"+request.toString());
                
                break;
            case "PARKINGPAYMENT":
                log.info("PARKINGPAYMENT.... STEP");
                if(input != null || !input.isEmpty()){
                    
                    String paymentMenu = "";
                    
                    if(request.getPaymentRequest() == null){
                    
                        ArrayList<ExistingVehiclesMenu> existingCars = request.getExisting();

                        if(Integer.parseInt(input) > existingCars.size()){
                            String errorMsg = "Try again\n";
                            request.setMessage(errorMsg + request.getMessage());
                        }else{
                            ExistingVehiclesMenu selectedVehicle = existingCars.get(Integer.parseInt(input)-1);

                            PaymentRequest paymentReq = new PaymentRequest();
                            paymentReq.setProductType("PARKING");
                            paymentReq.setPaymentChannel("EAZZYPAY");
                            paymentReq.setProductId(selectedVehicle.getVehicleRegistrationNum());
                            paymentReq.setAmount(selectedVehicle.getVehicleParkingAmount());
                            paymentReq.setMsisdn(request.getMsisdn());
                            paymentReq.setPaymentDescription("PARKING");
                            paymentReq.setTxnId(tools.generateId());

                            request.setPaymentRequest(paymentReq);

                            if(selectedVehicle.getOptionType().equals("REG")){
                                log.info("ProcessPayment:"+selectedVehicle.getVehicleRegistrationNum()+"|"+selectedVehicle.getOptionType());
                                paymentMenu = 
                                        String.format(tools.getMenu(Long.parseLong("25")),
                                                selectedVehicle.getVehicleParkingAmount(),
                                                selectedVehicle.getVehicleRegistrationNum()) 
                                        + mainAndBackParams;
                                MombasaCountyMenu nextMenu = 
                                            mombasaCountyMenuRepository
                                                    .findMombasaCountyMenuById(Long.parseLong("26"));
                                log.info("NEXT INPUT: PARKINGPAYMENT:"+nextMenu.toString());
                                request.setPreviousMenuLevel(request.getMenuLevel());
                                request.setMenuLevel(nextMenu.getId().toString());
                                request.setMenuNodeType(nextMenu.getParamType());
                                request.setMessage(paymentMenu);
                                request.setFlow(1);

                            }else if(selectedVehicle.getOptionType().equals("OPT")){
                                log.info("RegisterNewVehicle:"+selectedVehicle.getVehicleRegistrationNum()+"|"+selectedVehicle.getOptionType());

                                MombasaCountyMenu nextMenu = 
                                            mombasaCountyMenuRepository
                                                    .findMombasaCountyMenuById(Long.parseLong("18"));
                                
                                String newCarMenu = tools.getMenu(Long.parseLong("18"));
                                log.info("NEXT INPUT: PARKINGPAYMENT:"+nextMenu.toString());
                                request.setPreviousMenuLevel(request.getMenuLevel());
                                request.setMenuLevel(nextMenu.getId().toString());
                                request.setMenuNodeType(nextMenu.getParamType());
                                request.setMessage(newCarMenu);
                                request.setFlow(1);
                            }
                        }
                    }else{
                        paymentMenu = 
                                String.format(tools.getMenu(Long.parseLong("28")));
                        
                        //SaveTransation and call Payment
                        Payments pay = new Payments();
                        pay.setTransactionId(Long.parseLong(request.getPaymentRequest().getTxnId()));
                        pay.setProductId(request.getPaymentRequest().getProductId());
                        pay.setPaymentChannel(request.getPaymentRequest().getPaymentChannel());
                        pay.setPaymentDate(Calendar.getInstance().getTime());
                        pay.setRegisteredVehicle(registeredVehiclesRepository.findByvehicleRegistrationNumber(request.getPaymentRequest().getProductId()));
                        pay.setRegistredUsers(registeredUsersRepository.findBymsisdn(request.getMsisdn()));
                        pay.setAmount(new BigDecimal(request.getPaymentRequest().getAmount()));
                        paymentsRepository.save(pay);
                        
                        String apiResp = apiCall.inititePayment(request.getPaymentRequest());
                        
                        if(apiResp.equals("000")){
                            MombasaCountyMenu nextMenu = 
                                    mombasaCountyMenuRepository
                                            .findMombasaCountyMenuById(Long.parseLong("28"));
                            log.info("NEXT INPUT: PARKINGPAYMENT:"+nextMenu.toString());
                            request.setPreviousMenuLevel(request.getMenuLevel());
                            request.setMenuLevel(nextMenu.getId().toString());
                            request.setMenuNodeType(nextMenu.getParamType());
                            request.setMessage(paymentMenu);
                            request.setFlow(2);
                        }else{
                            request.setMessage(env.getProperty("msg-error"));
                            request.setFlow(2);
                        }
                        
                        request.setPaymentRequest(null);
                    }
                }else{
                    String errorMsg = "Try again\n";
                    request.setMessage(errorMsg + request.getMessage());   
                }
                break;
            
            case "ACCEPTPARKINGPAYMENT":
                MombasaCountyMenu nextMenu = 
                                        mombasaCountyMenuRepository
                                                .findMombasaCountyMenuById(Long.parseLong("28"));
                
                //SaveTransation and call Payment
                Payments pay = new Payments();
                pay.setTransactionId(Long.parseLong(request.getPaymentRequest().getTxnId()));
                pay.setProductId(request.getPaymentRequest().getProductId());
                pay.setPaymentChannel(request.getPaymentRequest().getPaymentChannel());
                pay.setPaymentDate(Calendar.getInstance().getTime());
                pay.setRegisteredVehicle(registeredVehiclesRepository.findByvehicleRegistrationNumber(request.getPaymentRequest().getProductId()));
                pay.setRegistredUsers(registeredUsersRepository.findBymsisdn(request.getMsisdn()));
                pay.setAmount(new BigDecimal(request.getPaymentRequest().getAmount()));
                paymentsRepository.save(pay);
                
                String apiResp = apiCall.inititePayment(request.getPaymentRequest());
                
                if(apiResp.equals("000")){
                    String newCarMenu = tools.getMenu(Long.parseLong("28"));
                    log.info("NEXT INPUT: PARKINGPAYMENT:"+nextMenu.toString());
                    request.setPreviousMenuLevel(request.getMenuLevel());
                    request.setMenuLevel(nextMenu.getId().toString());
                    request.setMenuNodeType(nextMenu.getParamType());
                    request.setMessage(newCarMenu);
                    request.setFlow(2);
                }else{
                    request.setMessage(env.getProperty("msg-error"));
                    request.setFlow(2);
                }
                
                break;
            case "SENDPAYMENT":
                break;
            /*
            case "CHANGEOLDPIN":
                break;
            case "CHANGENEWPIN":
                break;
            case "CHANGEPIN":
                break;*/
            /*case "PROCESSPAYMENT":
                if(input != null || !input.isEmpty()){
                    String menu = tools.getMenu(Long.parseLong(currentMenu.getChildNodeId())) + mainAndBackParams;

                    MombasaCountyMenu childMenu = 
                                    mombasaCountyMenuRepository
                                            .findMombasaCountyMenuById(Long.parseLong(currentMenu.getChildNodeId()));
                    log.info("NEXT INPUT: NewOption:"+childMenu.toString());
                    request.setPreviousMenuLevel(request.getMenuLevel());
                    request.setMenuLevel(childMenu.getId().toString());
                    request.setMenuNodeType(childMenu.getParamType());
                    request.setMessage(menu);
                    request.setFlow(1);
                }else{
                        
                }
                break;
            case "REJECTPAYMENT":
                break;
            case "STATEMENT":
                break;
            case "CHANGEOLDPIN":
                break;
            case "CHANGENEWPIN":
                break;
            case "CHANGEPIN":
                break;*/
                
        }
                
        
        return request;
    }
}
