/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.services;

import com.finserve.aggregator.models.Payments;
import com.finserve.aggregator.models.RegisteredUsers;
import com.finserve.aggregator.pojos.ApiResponse;
import com.finserve.aggregator.repositories.PaymentsRepository;
import com.finserve.aggregator.repositories.RegisteredUsersRepository;
import com.finserve.aggregator.utils.HttpUtils;
import com.google.gson.JsonObject;
import java.text.SimpleDateFormat;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 *
 * @author Wambayi
 */
@Service
public class StrathmoreApiCall {
    
    static Log log = LogFactory.getLog(FinserveApiCall.class.getName());
    
    @Autowired
    HttpUtils httpUtils;
    
    @Autowired
    Environment env;
    
    @Autowired
    PaymentsRepository paymentRepository;
    
    @Autowired
    RegisteredUsersRepository usersRepository;
    
    SimpleDateFormat regDateFmt = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat txnDateFmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    public void publishRegistrationInfo(Long userId){
        
        String url = env.getProperty("strathmore-api-reg-url");;
        
        JsonObject userInfo = new JsonObject();
        RegisteredUsers userData = usersRepository.findByUserId(userId); //findById(userId);
        
        userInfo.addProperty("msisdn", userData.getMsisdn());
        userInfo.addProperty("firstName", userData.getFirstName());
        userInfo.addProperty("lastName", userData.getLastName());
        userInfo.addProperty("idNumber", userData.getIdNumber());
        userInfo.addProperty("dateRegistered", regDateFmt.format(userData.getRegistrationDate()));
        
        log.info("PublishRegInfo: URL: "+url+", Data:"+userInfo.toString());
        
        try {
            ApiResponse resp = httpUtils.callApi(url, userInfo.toString(), "application/json");
            
            if(resp.getResponseCode() == 200){
                log.info("PublishRegInfo Response: "+resp.getResponseBody());
                
                userData.setSubmitted(1);
                usersRepository.save(userData);
            }else{
                log.info("PublishRegInfo Error....");
            }
            
        } catch (InterruptedException ex) {
            Logger.getLogger(StrathmoreApiCall.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ExecutionException ex) {
            Logger.getLogger(StrathmoreApiCall.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void publishTransactionInfo(Long txnId){
        
        String url = env.getProperty("strathmore-api-txn-url");;
        
        JsonObject paymentInfo = new JsonObject();
        
        Payments prodPayment = paymentRepository.findByTransactionId(txnId);
        
        paymentInfo.addProperty("service_category_id", "1");
        paymentInfo.addProperty("sale_id", txnId.toString());
        paymentInfo.addProperty("county_id", "1");
        paymentInfo.addProperty("subcounty_id", "4");
        paymentInfo.addProperty("customer_id", prodPayment.getRegistredUsers().getId());
        paymentInfo.addProperty("customer_first_name", prodPayment.getRegistredUsers().getFirstName());
        paymentInfo.addProperty("customer_last_name", prodPayment.getRegistredUsers().getLastName());
        paymentInfo.addProperty("transaction_mode_id", "1");
        paymentInfo.addProperty("amount", prodPayment.getAmount().toString());
        paymentInfo.addProperty("collector_id", "28");
        paymentInfo.addProperty("collector_name", "USSD, MOMBASA");
        paymentInfo.addProperty("fee_id", prodPayment.getRegisteredVehicle().getVehicleTypes().getFeeId());
        paymentInfo.addProperty("car_reg", prodPayment.getProductId());
        paymentInfo.addProperty("schedule_id", prodPayment.getRegisteredVehicle().getVehicleTypes().getScheduleId());
        paymentInfo.addProperty("part_id", prodPayment.getRegisteredVehicle().getVehicleTypes().getFeeId());
        paymentInfo.addProperty("sub_part_id", prodPayment.getRegisteredVehicle().getVehicleTypes().getSubPartId());
        paymentInfo.addProperty("ward_id", "1");
        paymentInfo.addProperty("long_coordinate", "0.0");
        paymentInfo.addProperty("lat_coordinate", "0.0");
        paymentInfo.addProperty("record_date", txnDateFmt.format(prodPayment.getPaymentDate()));
        
        log.info("PublishTxnInfo: URL: "+url+", Data:"+paymentInfo.toString());
        
        try {
            ApiResponse resp = httpUtils.callApi(url, paymentInfo.toString(), "application/json");
            
            if(resp.getResponseCode() == 200){
                log.info("PublishTxnInfo Response: "+resp.getResponseBody());
            }else{
                log.info("PublishTxnInfo Error....");
            }
            
        } catch (InterruptedException ex) {
            Logger.getLogger(StrathmoreApiCall.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ExecutionException ex) {
            Logger.getLogger(StrathmoreApiCall.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
