/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.services;

import com.finserve.aggregator.AggregatorGatewayApplication;
import com.finserve.aggregator.models.MombasaCountyMenu;
import com.finserve.aggregator.models.RegisteredUsers;
import com.finserve.aggregator.pojos.SessionData;
import com.finserve.aggregator.pojos.UssdRequest;
import com.finserve.aggregator.repositories.MombasaCountyMenuRepository;
import com.finserve.aggregator.utils.Tools;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.finserve.aggregator.repositories.RegisteredUsersRepository;

/**
 *
 * @author Wambayi
 */
@Service
public class UssdRequestProcessor {
    
    static Log log = LogFactory.getLog(UssdRequestProcessor.class.getName());
    
    @Autowired
    MombasaCountyMenuRepository mombasaCountyMenuRepository;
    
    @Autowired
    RegisteredUsersRepository registrationRepository;
    
    @Autowired
    Tools tools;
    
    @Autowired
    StateManagement stateMgt;
    
    public UssdRequest requestHandler(UssdRequest request){
        
        SessionManagement sessionMgr = AggregatorGatewayApplication.sessionMgr;
        SessionData sessionData = sessionMgr.getSessionData(request.getRequestId());
        
        String mainAndBackParams = "0. Main Menu";
        String mainAndCloseParams = "0. MainMenu";
        
        if(sessionData != null  && request.getMsisdn().equals(sessionData.getUssdRequest().getMsisdn())){
            log.info("SessionData:"+sessionData.getUssdRequest().toString());
            String newInput = tools.extractInput(request.getInput());
            
            if(newInput.equals("0")){
                return getMainMenu(sessionData.getUssdRequest());
            }else{
                /*if(newInput.equals("99")){
                    //Customer has selected back menu
                    MombasaCountyMenu prevMenuItem = 
                                        mombasaCountyMenuRepository
                                                .findMombasaCountyMenuById(Long.parseLong(sessionData.getUssdRequest().getPreviousMenuLevel()));
                    /*String menu = tools.getMenu(prevMenuItem.getId());
                    sessionData.getUssdRequest().setPreviousMenuLevel(prevMenuItem.getParentNodeId());
                    sessionData.getUssdRequest().setMenuLevel(prevMenuItem.getId().toString());
                    sessionData.getUssdRequest().setMenuNodeType(prevMenuItem.getParamType());
                    sessionData.getUssdRequest().setMessage(menu);
                    sessionData.getUssdRequest().setFlow(1);*/
                /*    sessionData.getUssdRequest().setMenuLevel(prevMenuItem.getId().toString());
                    sessionData.getUssdRequest().setMenuNodeType(prevMenuItem.getParamType());
                }*/
                
                switch(sessionData.getUssdRequest().getMenuNodeType()){
                    case "LIST":
                        //Get the option entered, check if valid and return new menu
                        //Check if input is valid
                        MombasaCountyMenu menuOptionSelected = 
                                mombasaCountyMenuRepository
                                        .findByParentNodeIdandInput(sessionData.getUssdRequest().getMenuLevel(), newInput);


                        if(menuOptionSelected != null){
                            log.info("LIST: SelectedOption: "+menuOptionSelected.toString());
                            String menu = tools.getMenu(Long.parseLong(menuOptionSelected.getChildNodeId())) + mainAndBackParams;

                            if(menuOptionSelected.getParamType().equals("OPTION") && !menuOptionSelected.getParamFunction().isEmpty()){
                                MombasaCountyMenu nextMenuItem = 
                                        mombasaCountyMenuRepository
                                                .findMombasaCountyMenuById(menuOptionSelected.getId());

                                sessionData.getUssdRequest().setInput(request.getInput());
                                sessionData.setUssdRequest(stateMgt
                                        .processRequest(
                                                sessionData.getUssdRequest(), 
                                                nextMenuItem));
                            }else if(menuOptionSelected.getParamType().equals("INPUT") && !menuOptionSelected.getParamFunction().isEmpty()){
                                MombasaCountyMenu nextMenuItem = 
                                        mombasaCountyMenuRepository
                                                .findMombasaCountyMenuById(menuOptionSelected.getId());

                                sessionData.getUssdRequest().setInput(request.getInput());
                                sessionData.setUssdRequest(stateMgt
                                        .processRequest(
                                                sessionData.getUssdRequest(), 
                                                nextMenuItem));
                            }else {

                                MombasaCountyMenu nextMenu = mombasaCountyMenuRepository
                                        .findMombasaCountyMenuById(Long.parseLong(menuOptionSelected.getChildNodeId()));
                                log.info("LIST->NEXT: NEXT Option: "+nextMenu.toString());
                                sessionData.getUssdRequest().setPreviousMenuLevel(sessionData.getUssdRequest().getMenuLevel());
                                sessionData.getUssdRequest().setMenuLevel(nextMenu.getId().toString());
                                sessionData.getUssdRequest().setMenuNodeType(nextMenu.getParamType());
                                sessionData.getUssdRequest().setMessage(menu);
                                sessionData.getUssdRequest().setFlow(1);
                            }
                            
                        }else{
                            String errorMenu = "Wrong input\n"+sessionData.getUssdRequest().getMessage();
                            sessionData.getUssdRequest().setMessage(errorMenu);
                            sessionData.getUssdRequest().setFlow(1);
                        }
                        
                        break;
                    case "INPUT":
                        //Save input, return next menu
                        //Check if input is valid
                        MombasaCountyMenu nextMenuItem = 
                                mombasaCountyMenuRepository
                                        .findMombasaCountyMenuById(Long.parseLong(sessionData.getUssdRequest().getMenuLevel()));

                        log.info("INPUT: CurrentOption:"+nextMenuItem.toString());

                        if(newInput != null){
                            String menu = tools.getMenu(Long.parseLong(nextMenuItem.getChildNodeId())) + mainAndBackParams;

                            if(nextMenuItem.getParamFunction() != null){
                                sessionData.getUssdRequest().setInput(request.getInput());
                                sessionData.setUssdRequest(stateMgt
                                        .processRequest(
                                                sessionData.getUssdRequest(), 
                                                nextMenuItem));
                            }else{

                                MombasaCountyMenu childMenu = 
                                    mombasaCountyMenuRepository
                                            .findMombasaCountyMenuById(Long.parseLong(nextMenuItem.getChildNodeId()));
                                log.info("NEXT INPUT: NewOption:"+childMenu.toString());
                                sessionData.getUssdRequest().setPreviousMenuLevel(sessionData.getUssdRequest().getMenuLevel());
                                sessionData.getUssdRequest().setMenuLevel(childMenu.getId().toString());
                                sessionData.getUssdRequest().setMenuNodeType(childMenu.getParamType());
                                sessionData.getUssdRequest().setMessage(menu);
                                sessionData.getUssdRequest().setFlow(1);
                            }
                        }else{
                            String errorMenu = "Wrong input\n"+sessionData.getUssdRequest().getMessage();
                            sessionData.getUssdRequest().setMessage(errorMenu);
                            sessionData.getUssdRequest().setFlow(1);
                        }
                        
                        break;
                    case "OPTION":
                        //Return next item
                        //Check if input is valid
                        MombasaCountyMenu nextMenu = 
                                mombasaCountyMenuRepository
                                        .findMombasaCountyMenuById(Long.parseLong(sessionData.getUssdRequest().getMenuLevel()));

                        log.info("OPTION: Current Option:"+nextMenu.toString());

                        if(newInput != null){
                            

                            if(nextMenu.getParamFunction() != null){
                                sessionData.getUssdRequest().setInput(request.getInput());
                                sessionData.setUssdRequest(stateMgt
                                        .processRequest(
                                                sessionData.getUssdRequest(), 
                                                nextMenu));
                            }else{
                                String menu = tools.getNextMenuItem(Long.parseLong(nextMenu.getChildNodeId())) + mainAndBackParams;
                                
                                MombasaCountyMenu childMenu = 
                                    mombasaCountyMenuRepository
                                            .findMombasaCountyMenuById(Long.parseLong(nextMenu.getChildNodeId()));

                                log.info("NEXT OPTION: NEW Option:"+childMenu.toString());
                                sessionData.getUssdRequest().setPreviousMenuLevel(sessionData.getUssdRequest().getMenuLevel());
                                sessionData.getUssdRequest().setMenuLevel(childMenu.getId().toString());
                                sessionData.getUssdRequest().setMenuNodeType(childMenu.getParamType());
                                sessionData.getUssdRequest().setMessage(menu);
                                sessionData.getUssdRequest().setFlow(1);
                            }
                        }else{
                            String errorMenu = "Wrong input\n"+sessionData.getUssdRequest().getMessage();
                            sessionData.getUssdRequest().setMessage(errorMenu);
                            sessionData.getUssdRequest().setFlow(1);
                        }
                        
                        break;
                    case "DYNAMIC":
                        
                        if(newInput != null){
                            
                            MombasaCountyMenu currentMenu = 
                                mombasaCountyMenuRepository
                                        .findMombasaCountyMenuById(Long.parseLong(sessionData.getUssdRequest().getMenuLevel()));

                            sessionData.getUssdRequest().setInput(request.getInput());
                            sessionData.setUssdRequest(stateMgt
                                    .processRequest(
                                            sessionData.getUssdRequest(), 
                                            currentMenu));
                        }else{
                            String errorMenu = "Wrong input\n"+sessionData.getUssdRequest().getMessage();
                            sessionData.getUssdRequest().setMessage(errorMenu);
                            sessionData.getUssdRequest().setFlow(1);
                        }
                        
                    case "END":
                        //WIll this really do anything?
                        break;
                }
            }
            
        }else{
            //Check if customer is registered
            RegisteredUsers regUser = registrationRepository.findBymsisdn(request.getMsisdn());
            
            sessionData = new SessionData();
            
            if(regUser !=null){
                //User is registered, Return the Services Menu
                String userName = regUser.getFirstName();
                String menu = tools.getMenu(new Long(12));
                request.setMenuLevel("12");
                request.setMenuNodeType(mombasaCountyMenuRepository.findMombasaCountyMenuById(new Long(12)).getParamType());
                request.setMessage(String.format(menu, userName));
                request.setFlow(1);
            }else{
                //User is not registered, return the registration menu
                String menu = tools.getMenu(new Long(2));
                request.setMenuLevel("2");
                request.setMenuNodeType(mombasaCountyMenuRepository.findMombasaCountyMenuById(new Long(2)).getParamType());
                request.setMessage(menu);
                request.setFlow(1);
            }
            
            sessionData.setUssdRequest(request);
            sessionMgr.newSession(request.getRequestId(), sessionData);
        }
        
        return sessionData.getUssdRequest();
    }
    
    
    private UssdRequest getMainMenu(UssdRequest request){
        
        RegisteredUsers regUser = registrationRepository.findBymsisdn(request.getMsisdn());
            
        if(regUser !=null){
            //User is registered, Return the Services Menu
            String userName = regUser.getFirstName();
            String menu = tools.getMenu(new Long(12));
            request.setMenuLevel("12");
            request.setMenuNodeType(mombasaCountyMenuRepository.findMombasaCountyMenuById(new Long(12)).getParamType());
            request.setMessage(String.format(menu, userName));
            request.setFlow(1);
        }else{
            //User is not registered, return the registration menu
            String menu = tools.getMenu(new Long(2));
            request.setMenuLevel("2");
            request.setMenuNodeType(mombasaCountyMenuRepository.findMombasaCountyMenuById(new Long(2)).getParamType());
            request.setMessage(menu);
            request.setFlow(1);
        }
        
        return request;
    }
    
}
