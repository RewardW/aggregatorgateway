/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.services;

import com.finserve.aggregator.models.Payments;
import com.finserve.aggregator.pojos.ApiResponse;
import com.finserve.aggregator.pojos.PaymentRequest;
import com.finserve.aggregator.repositories.PaymentsRepository;
import com.finserve.aggregator.utils.ApiCall;
import com.finserve.aggregator.utils.HttpUtils;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Wambayi
 */
@Service
public class FinserveApiCall {
    
    static Log log = LogFactory.getLog(FinserveApiCall.class.getName());
    
    @Autowired
    Environment env;
    
    @Autowired
    HttpUtils httpUtils;
    
    @Autowired
    StrathmoreApiCall strathApiCall;
    
    @Autowired
    PaymentsRepository paymentRepository;
        
    public String inititePayment(PaymentRequest paymentReq){
        
        log.info("PaymentRequest:"+paymentReq.toString());
        
        String url = env.getProperty("finserve-api-payment-url");
        String username = env.getProperty("finserve-api-merchant");
        String password = env.getProperty("finserve-api-password");
        
        String body = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:equ=\"http://EquitySTKkenya.integration.mb.modefinserver.com/\">\n" +
"        <soapenv:Header/>\n" +
"        <soapenv:Body>\n" +
"          <equ:paymentRequest>\n" +
"            <UserName>"+username+"</UserName>\n" +
"            <Password>"+password+"</Password>\n" +
"            <Amount>"+paymentReq.getAmount()+"</Amount>\n" +
"            <MobileNumber>"+paymentReq.getMsisdn()+"</MobileNumber>\n" +
"            <Description>"+paymentReq.getPaymentDescription()+"</Description>\n" +
"            <TransactionType>"+paymentReq.getProductType()+"</TransactionType>\n" +
"            <AuditNumber>"+paymentReq.getTxnId()+"</AuditNumber>\n" +
"            <callback_url></callback_url>\n" +
"            <APICallbackurl></APICallbackurl>\n" +
"          </equ:paymentRequest>\n" +
"        </soapenv:Body>\n" +
"      </soapenv:Envelope>";
        
        try {
            
            log.info("PaymentAPICall: "+url+" | "+body);
            
            ApiResponse response = httpUtils.callApi(url, body,"application/xml");
            
            if(response.getResponseCode() == 200){
            
                DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = domFactory.newDocumentBuilder();
                Document doc = builder.parse(new InputSource(new ByteArrayInputStream(response.getResponseBody().getBytes("utf-8"))));
                
                NodeList nList = doc.getElementsByTagName("S:Envelope");
                Element eElement = (Element) nList.item(0);
                NodeList bodyList = eElement.getElementsByTagName("S:Body");
                Element bodyParams =  (Element) bodyList.item(0);
                NodeList genenericList = bodyParams.getElementsByTagName("ns2:processingrequestingResponse");
                Element genericParams =  (Element) genenericList.item(0);
                NodeList returnList = genericParams.getElementsByTagName("MFSPayments");
                Element returnParams =  (Element) returnList.item(0);
                String statusCode = returnParams.getElementsByTagName("Status").item(0).getTextContent();
                String responseCode = returnParams.getElementsByTagName("ResultCode").item(0).getTextContent();
                if(responseCode.equals("000")){
                    
                    String referenceNum = returnParams.getElementsByTagName("TransactionRefNo").item(0).getTextContent();
                    Payments pay = paymentRepository.findByTransactionId(Long.parseLong(paymentReq.getTxnId()));
                    
                    pay.setStatus("pending");
                    pay.setPaymentReferenceId(referenceNum);
                    paymentRepository.save(pay);
                    
                    
                    log.info("MFS:txnStatus:"+statusCode+"| Result"+responseCode+"|Success");
                    
                    return "000";
                }else{
                    
                    Payments pay = paymentRepository.findByTransactionId(Long.parseLong(paymentReq.getTxnId()));
                    pay.setStatus("failed");
                    paymentRepository.save(pay);
                    
                    System.out.println("MFS:txnStatus:"+statusCode+"| Result"+responseCode +"|Failed");
                    return "500";
                }
            
            }else{
               return "500"; 
            }
            
        } catch (InterruptedException ex) {
            Logger.getLogger(FinserveApiCall.class.getName()).log(Level.SEVERE, null, ex);
            return "500"; 
        } catch (ExecutionException ex) {
            Logger.getLogger(FinserveApiCall.class.getName()).log(Level.SEVERE, null, ex);
            return "500"; 
        } catch (SAXException ex) {
            Logger.getLogger(FinserveApiCall.class.getName()).log(Level.SEVERE, null, ex);
            return "500"; 
        } catch (IOException ex) {
            Logger.getLogger(FinserveApiCall.class.getName()).log(Level.SEVERE, null, ex);
            return "500"; 
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(FinserveApiCall.class.getName()).log(Level.SEVERE, null, ex);
            return "500"; 
        }
    }
    
    public void getTxnStatus(Long txnId,String referenceId){
        String username = env.getProperty("finserve-api-merchant");
        String url = env.getProperty("finserve-api-status-url");
        
        String body = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' "
                + "xmlns:equ='http://EquitySTKkenya.integration.mb.modefinserver.com/'>\n" +
                "<soapenv:Header/>\n" +
                "<soapenv:Body>\n" +
                "<equ:TransactionStatus>\n" +
                "<UserName>"+username+"</UserName>\n" +
                "<MobileNumber/>\n" +
                "<AuditNumber>"+referenceId+"</AuditNumber>\n" +
                "</equ:TransactionStatus>\n" +
                "</soapenv:Body>\n" +
                "</soapenv:Envelope>";
        
        try{
            log.info("GetTxnStatus: "+url+" | "+body);
            
            ApiResponse response = httpUtils.callApi(url, body,"application/xml");
            
            if(response.getResponseCode() == 200){
            
                DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = domFactory.newDocumentBuilder();
                Document doc = builder.parse(new InputSource(new ByteArrayInputStream(response.getResponseBody().getBytes("utf-8"))));
                
                NodeList nList = doc.getElementsByTagName("S:Envelope");
                Element eElement = (Element) nList.item(0);
                NodeList bodyList = eElement.getElementsByTagName("S:Body");
                Element bodyParams =  (Element) bodyList.item(0);
                NodeList genenericList = bodyParams.getElementsByTagName("ns2:TransactionStatusResponse");
                Element genericParams =  (Element) genenericList.item(0);
                NodeList returnList = genericParams.getElementsByTagName("TransactionStatus");
                Element returnParams =  (Element) returnList.item(0);
                String statusCode = returnParams.getElementsByTagName("Status").item(0).getTextContent();
                String responseCode = returnParams.getElementsByTagName("ResultCode").item(0).getTextContent();
                String result = returnParams.getElementsByTagName("Result").item(0).getTextContent();

                if(result.equals("0")){
                    //Update payment status
                    Payments pay = paymentRepository.findByTransactionId(txnId);
                    pay.setStatus("success");
                    paymentRepository.save(pay);
                    //Notify Strathmore
                    strathApiCall.publishTransactionInfo(txnId);
                }else{
                    Payments pay = paymentRepository.findByTransactionId(txnId);
                    pay.setStatus("failed");
                    paymentRepository.save(pay);
                }
            }
            
        } catch (InterruptedException ex) {
            Logger.getLogger(FinserveApiCall.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ExecutionException ex) {
            Logger.getLogger(FinserveApiCall.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(FinserveApiCall.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FinserveApiCall.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(FinserveApiCall.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FinserveApiCall.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
