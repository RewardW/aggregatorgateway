package com.finserve.aggregator;

import com.finserve.aggregator.services.SessionManagement;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class AggregatorGatewayApplication {
        
    //Object to store session parameters    
    public static SessionManagement sessionMgr;
    
    public static void main(String[] args) {
            SpringApplication.run(AggregatorGatewayApplication.class, args);
            
            sessionMgr = new SessionManagement();
    }
}
