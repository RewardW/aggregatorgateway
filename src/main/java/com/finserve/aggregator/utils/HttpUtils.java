/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.utils;

import com.finserve.aggregator.pojos.ApiResponse;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 *
 * @author Wambayi
 */
@Service
public class HttpUtils {
    static Log log = LogFactory.getLog(HttpUtils.class.getName());
    
    @Autowired
    Environment env;
    
    PoolingHttpClientConnectionManager connManager;
    CloseableHttpClient client;

    public HttpUtils() {
        this.connManager = new PoolingHttpClientConnectionManager();
        connManager.setDefaultMaxPerRoute(10);
        connManager.setMaxTotal(10);
        this.client = HttpClients.custom()
            .setConnectionManager(connManager)
            .build();
    }
    
    ExecutorService executorService = Executors.newFixedThreadPool(10);
    
    public ApiResponse callApi(String url, String body, String contentType) throws InterruptedException, ExecutionException{
        Future future = executorService.submit(new ApiCall(client,url, body,contentType));
        ApiResponse result = (ApiResponse) future.get();
        return result;
    }
}
