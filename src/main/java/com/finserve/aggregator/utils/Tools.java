/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.utils;

import com.finserve.aggregator.models.MombasaCountyMenu;
import com.finserve.aggregator.repositories.MombasaCountyMenuRepository;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Wambayi
 */
@Service
public class Tools {
    
    static Log log = LogFactory.getLog(Tools.class.getName());
    
    @Autowired
    MombasaCountyMenuRepository mombasaCountyMenuRepository;
    
    public String getMenu(Long paramId){
        String menu = "";
        
        MombasaCountyMenu menuItem = mombasaCountyMenuRepository.findMombasaCountyMenuById(paramId);
        
        menu = menuItem.getParamText()+"\n";
        
        if(menuItem.getParamType().equals("LIST")||menuItem.getParamType().equals("DYNAMIC")){
            List<MombasaCountyMenu> menuItems = mombasaCountyMenuRepository.findByparentNodeId(menuItem.getId().toString());
            
            for(MombasaCountyMenu msaMenu: menuItems){
                menu+=msaMenu.getParamTextNum()+". "+msaMenu.getParamText()+"\n";
            }
        }
        
        return menu;
    }
    
    public String getNextMenuItem(Long paramId){
        String menu = "";
        
        List<MombasaCountyMenu> menuItems = mombasaCountyMenuRepository.findByparentNodeId(paramId.toString());
            
        for(MombasaCountyMenu msaMenu: menuItems){
            menu+=msaMenu.getParamTextNum()+". "+msaMenu.getParamText()+"\n";
        }
    
        return menu;
    }
    
    public MombasaCountyMenu getNextMenu(Long paramId){
        List<MombasaCountyMenu> menuItems = mombasaCountyMenuRepository.findByparentNodeId(paramId.toString());
        
        return menuItems.get(0);
    }
    
    public String extractInput(String input){
        //Returns input the user just entered
        String[] params = input.split("\\*");
        String newInput =  params[params.length - 1];
        
        log.info("NewInput: "+newInput);
        
        return newInput;
    }
    
    public String generateId(){
        String id = "";
        int min = 10001;
        int max = 99999;
        int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);

        Calendar calendar = Calendar.getInstance();
        String idparam = Long.toString(calendar.getTimeInMillis());
        String firstPart = idparam.substring(4, idparam.length());

        id = 1+firstPart + randomNum;

        return id;
    }
}
