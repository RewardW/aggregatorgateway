/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.utils;

import com.finserve.aggregator.pojos.ApiResponse;
import java.io.IOException;
import java.util.concurrent.Callable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author Wambayi
 */

public class ApiCall  implements Callable<ApiResponse>{
    
    static Log log = LogFactory.getLog(ApiCall.class.getName());
    String response;
    String url;
    String requestBody;
    String contentType;
    private final CloseableHttpClient httpClient;

    public ApiCall(CloseableHttpClient httpClient,String url, String requestBody,String contentType) {
        this.httpClient = httpClient;
        this.url = url;
        this.requestBody = requestBody;
        this.contentType = contentType;
    }

    @Override
    public ApiResponse call(){
        HttpGet httpGet;
        HttpPost httpPost;
        CloseableHttpResponse httpResponse = null;
        
        ApiResponse response = new ApiResponse();
        try {
            if(requestBody == null){
                httpGet = new HttpGet(url);
                httpResponse = httpClient.execute(httpGet);
            }else{
                httpPost = new HttpPost(url);
                httpPost.addHeader("Content-Type" , contentType);
                httpPost.setEntity(new StringEntity(requestBody));
                httpResponse = httpClient.execute(httpPost);
            }
            HttpEntity entity = httpResponse.getEntity();
            
            StatusLine respCode = httpResponse.getStatusLine();
            String e4tResp = EntityUtils.toString(entity);
            
            log.info("EffortelResp: Status:"+respCode.getStatusCode()+"| Message:"+e4tResp);
            
            if(respCode.getStatusCode() == 200){
                response.setResponseCode(200);
                response.setResponseBody(e4tResp);
            }else{
                response.setResponseCode(500);
                response.setResponseBody(null);
            }      
        } catch (IOException ex) {
            log.error(ex);
            response.setResponseCode(500);
            response.setResponseBody(null);
        }

        return response;
    }
}
