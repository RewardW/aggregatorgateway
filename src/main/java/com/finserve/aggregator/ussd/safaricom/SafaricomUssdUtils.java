/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.ussd.safaricom;

import com.finserve.aggregator.pojos.UssdRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 *
 * @author Wambayi
 */
@Service
public class SafaricomUssdUtils {
    @Autowired
    Environment env;
    
    static Log log = LogFactory.getLog(SafaricomUssdUtils.class.getName());
    
    public String ussdResponse(UssdRequest req){
        
        String response = null;
        
        if(req.getFlow() == 1){
            
            response = "CON "+req.getMessage();
            
        }else if(req.getFlow() == 2){
            
            response = "END "+req.getMessage();
            
        }else{
            response = "END "+req.getMessage();
        }
        
        log.info("USSD Response:"+response);
        
        return response;
    }
}
