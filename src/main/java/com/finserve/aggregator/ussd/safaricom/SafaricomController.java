/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.ussd.safaricom;

import com.finserve.aggregator.pojos.UssdRequest;
import com.finserve.aggregator.services.UssdRequestProcessor;
import java.util.Collection;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Wambayi
 */
@RestController
//@RequestMapping("/county")
public class SafaricomController {
    @Autowired
    SafaricomUssdUtils ussdUtils;

    @Autowired
    UssdRequestProcessor ussdRequestProcessor;

    static Log log = LogFactory.getLog(SafaricomController.class.getName());

    @GetMapping(value="/county/ussd/safaricom")
    public  String ussd(@RequestParam("ORIG") String msisdn,
            @RequestParam("SESSION_ID") String sessionId,
            @RequestParam("DEST") String shortCode,
            @RequestParam("USSD_PARAMS") String input){
        
        log.info("SafcomRawRequest: ORIG->"+msisdn+",SESSION_ID->"+sessionId+",DEST->"+shortCode+",USSD_PARAMS->"+input);
        
        String params = "SafcomRawRequest: ORIG->"+msisdn+",SESSION_ID->"+sessionId+",DEST->"+shortCode+",USSD_PARAMS->"+input;
        
        UssdRequest request = new UssdRequest();
        request.setMsisdn(msisdn);
        request.setInput(input);
        request.setRequestId(sessionId);
        request.setStarCode(shortCode);
    
        log.info("SafcomParams: "+request.toString());

        return ussdUtils.ussdResponse(ussdRequestProcessor.requestHandler(request));
    }
    
    @GetMapping(value = "/county/user")
    public HashMap<String, String> getitem(){
        HashMap<String, String> map  = new HashMap<>();
        log.info("Something: test is working....");
        map.put("Resp", "Something: test is working");
        return  map;// "There is something here...";
    }
    
    @RequestMapping(value="/hello")
    @ResponseBody
    public Collection<String> sayHello() {
        return IntStream.range(0, 10)
          .mapToObj(i -> "Hello number " + i)
          .collect(Collectors.toList());
    }
    
    /*
    @ResponseBody
    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    public String handleHttpMediaTypeNotAcceptableException() {
        return "acceptable MIME type:" + MediaType.TEXT_PLAIN_VALUE;
    }*/
}
