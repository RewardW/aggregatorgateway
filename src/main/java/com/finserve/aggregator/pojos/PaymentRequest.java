/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.pojos;

/**
 *
 * @author Wambayi
 */
public class PaymentRequest {
    
    private String productType;
    private String productId;
    private String amount;
    private String paymentChannel;
    private String msisdn;
    private String paymentDescription;
    private String txnId;

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPaymentDescription() {
        return paymentDescription;
    }

    public void setPaymentDescription(String paymentDescription) {
        this.paymentDescription = paymentDescription;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    @Override
    public String toString() {
        return "PaymentRequest{" + "productType=" + productType + ", productId=" + productId + ", amount=" + amount + ", paymentChannel=" + paymentChannel + ", msisdn=" + msisdn + ", paymentDescription=" + paymentDescription + ", txnId=" + txnId + '}';
    }

    
}
