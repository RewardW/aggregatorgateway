/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.pojos;

import java.util.ArrayList;

/**
 *
 * @author Wambayi
 */
public class UssdRequest {
    
    String requestId;
    String msisdn;
    String timestamp;
    String starCode;
    String input;
    String message;
    String requestType;
    RegistrationData registrationData;
    private String menuLevel; //What part of the menu are we on?
    private String parentNodeId;
    private String menuNodeType;
    private String previousMenuLevel;
    private int flow; //Continuing session or end
    VehicleRegistations vehicleReg;
    ArrayList<ExistingVehiclesMenu> existing;
    PaymentRequest paymentRequest;
    
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getStarCode() {
        return starCode;
    }

    public void setStarCode(String starCode) {
        this.starCode = starCode;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getMenuLevel() {
        return menuLevel;
    }

    public void setMenuLevel(String menuLevel) {
        this.menuLevel = menuLevel;
    }

    public int getFlow() {
        return flow;
    }

    public void setFlow(int flow) {
        this.flow = flow;
    }

    public String getParentNodeId() {
        return parentNodeId;
    }

    public void setParentNodeId(String parentNodeId) {
        this.parentNodeId = parentNodeId;
    }

    public String getMenuNodeType() {
        return menuNodeType;
    }

    public void setMenuNodeType(String menuNodeType) {
        this.menuNodeType = menuNodeType;
    }

    public String getPreviousMenuLevel() {
        return previousMenuLevel;
    }

    public void setPreviousMenuLevel(String previousMenuLevel) {
        this.previousMenuLevel = previousMenuLevel;
    }

    public RegistrationData getRegistrationData() {
        return registrationData;
    }

    public void setRegistrationData(RegistrationData registrationData) {
        this.registrationData = registrationData;
    }

    public VehicleRegistations getVehicleReg() {
        return vehicleReg;
    }

    public void setVehicleReg(VehicleRegistations vehicleReg) {
        this.vehicleReg = vehicleReg;
    }

    public ArrayList<ExistingVehiclesMenu> getExisting() {
        return existing;
    }

    public void setExisting(ArrayList<ExistingVehiclesMenu> existing) {
        this.existing = existing;
    }

    public PaymentRequest getPaymentRequest() {
        return paymentRequest;
    }

    public void setPaymentRequest(PaymentRequest paymentRequest) {
        this.paymentRequest = paymentRequest;
    }
    

    @Override
    public String toString() {
        return "UssdRequest{" + "requestId=" + requestId + ", msisdn=" + msisdn + 
                ", timestamp=" + timestamp + ", starCode=" + starCode + 
                ", input=" + input + ", message=" + message + ", requestType=" + requestType + 
                ", registrationData=" + registrationData + ", menuLevel=" + menuLevel + 
                ", parentNodeId=" + parentNodeId + ", menuNodeType=" + menuNodeType + 
                ", previousMenuLevel=" + previousMenuLevel + ", flow=" + flow + 
                ", vehicleReg=" + vehicleReg + ", paymentRequest=" + paymentRequest +'}';
    }

    
    
}
