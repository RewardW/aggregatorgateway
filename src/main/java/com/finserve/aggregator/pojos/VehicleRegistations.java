/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.pojos;

/**
 *
 * @author Wambayi
 */
public class VehicleRegistations {
    
    private int vehicleTypeID;
    private String vehicleRegNum;

    public int getVehicleTypeID() {
        return vehicleTypeID;
    }

    public void setVehicleTypeID(int vehicleTypeID) {
        this.vehicleTypeID = vehicleTypeID;
    }

    public String getVehicleRegNum() {
        return vehicleRegNum;
    }

    public void setVehicleRegNum(String vehicleRegNum) {
        this.vehicleRegNum = vehicleRegNum;
    }

    @Override
    public String toString() {
        return "VehicleRegistations{" + "vehicleTypeID=" + vehicleTypeID + ", vehicleRegNum=" + vehicleRegNum + '}';
    }
    
}
