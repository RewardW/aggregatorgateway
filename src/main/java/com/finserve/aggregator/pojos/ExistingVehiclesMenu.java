/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.pojos;

/**
 *
 * @author Wambayi
 */
public class ExistingVehiclesMenu {
    
    int menuOptionId;
    String vehicleRegistrationNum;
    String vehicleParkingAmount;
    String optionType; //Vehicle or Menu Option

    public int getMenuOptionId() {
        return menuOptionId;
    }

    public void setMenuOptionId(int menuOptionId) {
        this.menuOptionId = menuOptionId;
    }

    public String getVehicleRegistrationNum() {
        return vehicleRegistrationNum;
    }

    public void setVehicleRegistrationNum(String vehicleRegistrationNum) {
        this.vehicleRegistrationNum = vehicleRegistrationNum;
    }

    public String getVehicleParkingAmount() {
        return vehicleParkingAmount;
    }

    public void setVehicleParkingAmount(String vehicleParkingAmount) {
        this.vehicleParkingAmount = vehicleParkingAmount;
    }

    public String getOptionType() {
        return optionType;
    }

    public void setOptionType(String optionType) {
        this.optionType = optionType;
    }

    @Override
    public String toString() {
        return "ExistingVehiclesMenu{" + "menuOptionId=" + menuOptionId + ", vehicleRegistrationNum=" + vehicleRegistrationNum + ", vehicleParkingAmount=" + vehicleParkingAmount + ", optionType=" + optionType + '}';
    }
    
}
