/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Wambayi
 */
@Entity
@Table(name = "Payments")
public class Payments {
    
    @Id
    @Column(name = "transactionId")
    private Long transactionId;
    @ManyToOne()
    @JoinColumn(name="UserId",nullable=false, updatable = true, insertable = true)
    private RegisteredUsers registredUsers;
    private String productId; //Product identifier (vehicles, land rates, permits)
    private Date paymentDate;
    private BigDecimal amount;
    private String paymentChannel;
    private String paymentReferenceId;
    @ManyToOne()
    @JoinColumn(name="vehicleId",nullable=true, updatable = true, insertable = true)
    private RegisteredVehicles registeredVehicle;
    private String status;
    private String message;

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public RegisteredUsers getRegistredUsers() {
        return registredUsers;
    }

    public void setRegistredUsers(RegisteredUsers registredUsers) {
        this.registredUsers = registredUsers;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public String getPaymentReferenceId() {
        return paymentReferenceId;
    }

    public void setPaymentReferenceId(String paymentReferenceId) {
        this.paymentReferenceId = paymentReferenceId;
    }

    public RegisteredVehicles getRegisteredVehicle() {
        return registeredVehicle;
    }

    public void setRegisteredVehicle(RegisteredVehicles registeredVehicle) {
        this.registeredVehicle = registeredVehicle;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Payments{" + "id=" + transactionId + ", registredUsers=" + registredUsers + ", productId=" + productId + ", paymentDate=" + paymentDate + ", amount=" + amount + ", paymentChannel=" + paymentChannel + ", paymentReferenceId=" + paymentReferenceId + ", registeredVehicle=" + registeredVehicle + ", status=" + status + ", message=" + message + '}';
    }
}
