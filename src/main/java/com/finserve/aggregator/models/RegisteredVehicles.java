/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Wambayi
 */
@Entity
@Table(name = "RegisteredVehicles", uniqueConstraints = { @UniqueConstraint(columnNames = { "vehicleRegistrationNumber" })})
public class RegisteredVehicles {
    
    @Id
    @Column(name = "vehicleRegistrationNumber")
    private String vehicleRegistrationNumber;
    
    @ManyToOne()
    @JoinColumn(name="UserId",nullable=false, updatable = true, insertable = true)
    private RegisteredUsers registredUsers;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;
    
    @ManyToOne()
    @JoinColumn(name="vehicleTypeId",nullable=false, updatable = true, insertable = true)
    private VehicleTypes vehicleTypes;

    public RegisteredUsers getRegistredUsers() {
        return registredUsers;
    }

    public void setRegistredUsers(RegisteredUsers registredUsers) {
        this.registredUsers = registredUsers;
    }

    public String getVehicleRegistrationNumber() {
        return vehicleRegistrationNumber;
    }

    public void setVehicleRegistrationNumber(String vehicleRegistrationNumber) {
        this.vehicleRegistrationNumber = vehicleRegistrationNumber;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public VehicleTypes getVehicleTypes() {
        return vehicleTypes;
    }

    public void setVehicleTypes(VehicleTypes vehicleTypes) {
        this.vehicleTypes = vehicleTypes;
    }

    @Override
    public String toString() {
        return "RegisteredVehicles{" + "vehicleRegistrationNumber=" + vehicleRegistrationNumber + ", registredUsers=" + registredUsers + ", registrationDate=" + registrationDate + ", vehicleTypes=" + vehicleTypes + '}';
    }

    
    
}
