/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Wambayi
 */
@Entity
@Table(name = "MombasaCountyMenu")
public class MombasaCountyMenu {

    @Id
    private Long id;
    private String paramName;
    private String parentNodeId;
    private String childNodeId;
    private String paramText;
    private String paramTextNum;
    private String paramType;
    private String paramFunction;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParentNodeId() {
        return parentNodeId;
    }

    public void setParentNodeId(String parentNodeId) {
        this.parentNodeId = parentNodeId;
    }

    public String getChildNodeId() {
        return childNodeId;
    }

    public void setChildNodeId(String childNodeId) {
        this.childNodeId = childNodeId;
    }

    public String getParamText() {
        return paramText;
    }

    public void setParamText(String paramText) {
        this.paramText = paramText;
    }

    public String getParamTextNum() {
        return paramTextNum;
    }

    public void setParamTextNum(String paramTextNum) {
        this.paramTextNum = paramTextNum;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getParamFunction() {
        return paramFunction;
    }

    public void setParamFunction(String paramFunction) {
        this.paramFunction = paramFunction;
    }

    @Override
    public String toString() {
        return "MombasaCountyMenu{" + "id=" + id + ", paramName=" + paramName + ", parentNodeId=" + parentNodeId + ", childNodeId=" + childNodeId + ", paramText=" + paramText + ", paramTextNum=" + paramTextNum + ", paramType=" + paramType + ", paramFunction=" + paramFunction + '}';
    }
    
    
    
}
