/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.models;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Wambayi
 */
@Entity
@Table(name = "VehiclesTypes")
public class VehicleTypes {
    
    @Id
    @Column(name = "vehicleTypeId")
    private Long vehicleTypeId;
    private String vehicleTypeName;
    private BigDecimal parkingFee;
    private int feeId;
    private int scheduleId;
    private int partId;
    private int subPartId;

    public Long getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(Long vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public BigDecimal getParkingFee() {
        return parkingFee;
    }

    public void setParkingFee(BigDecimal parkingFee) {
        this.parkingFee = parkingFee;
    }

    public int getFeeId() {
        return feeId;
    }

    public void setFeeId(int feeId) {
        this.feeId = feeId;
    }

    public int getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(int scheduleId) {
        this.scheduleId = scheduleId;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public int getSubPartId() {
        return subPartId;
    }

    public void setSubPartId(int subPartId) {
        this.subPartId = subPartId;
    }

    @Override
    public String toString() {
        return "VehicleTypes{" + "vehicleTypeId=" + vehicleTypeId + ", vehicleTypeName=" + vehicleTypeName + ", parkingFee=" + parkingFee + ", feeId=" + feeId + ", scheduleId=" + scheduleId + ", partId=" + partId + ", subPartId=" + subPartId + '}';
    }
    
}
