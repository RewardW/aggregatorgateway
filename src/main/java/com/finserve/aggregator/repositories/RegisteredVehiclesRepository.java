/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.repositories;

import com.finserve.aggregator.models.RegisteredUsers;
import com.finserve.aggregator.models.RegisteredVehicles;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Wambayi
 */
public interface RegisteredVehiclesRepository  extends PagingAndSortingRepository<RegisteredVehicles, Long> {
    
    List<RegisteredVehicles> findRegisteredVehiclesByRegistredUsers(RegisteredUsers registeredUser);
    
    RegisteredVehicles findByvehicleRegistrationNumber(String vehicleRegNum);
    
}
