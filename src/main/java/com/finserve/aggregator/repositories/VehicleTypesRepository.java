/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.repositories;

import com.finserve.aggregator.models.VehicleTypes;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Wambayi
 */
public interface VehicleTypesRepository extends PagingAndSortingRepository<VehicleTypes, Long> {
    
    //@Override
    VehicleTypes findByvehicleTypeId(Long typeId);
}
