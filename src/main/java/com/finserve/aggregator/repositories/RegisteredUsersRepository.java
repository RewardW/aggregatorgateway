/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.repositories;

import com.finserve.aggregator.models.RegisteredUsers;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Wambayi
 */
public interface RegisteredUsersRepository  extends PagingAndSortingRepository<RegisteredUsers, Long> {
    
    RegisteredUsers findBymsisdn(String msisdn);
    
    //@Override
    //RegisteredUsers findByUserId(Long id);
    
    @Query("select r from RegisteredUsers r where id = :id")
	RegisteredUsers findByUserId(@Param("id") Long id);
}
