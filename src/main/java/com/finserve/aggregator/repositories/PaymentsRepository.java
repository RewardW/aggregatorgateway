/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.repositories;

import com.finserve.aggregator.models.Payments;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Wambayi
 */
public interface PaymentsRepository extends PagingAndSortingRepository<Payments, Long> {
    Payments findByTransactionId(Long id);
}
