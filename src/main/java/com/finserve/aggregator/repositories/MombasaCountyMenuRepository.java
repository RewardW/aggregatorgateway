/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator.repositories;

import com.finserve.aggregator.models.MombasaCountyMenu;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Wambayi
 */
public interface MombasaCountyMenuRepository extends PagingAndSortingRepository<MombasaCountyMenu, Long> {
    
    // using this query in qualificationCheck, no transaction required
    //@Query("select c from Customer c where subscriberNumber = :msisdn")
    //MombasaCountyMenu findByPhoneNumber(@Param("msisdn") String msisdn);
    
    //Get the element and option selected
    @Query("select m from MombasaCountyMenu m where parentNodeId = :parentNode and paramTextNum = :input")
            MombasaCountyMenu findByParentNodeIdandInput(@Param("parentNode") String parentNode,
                    @Param("input") String input);
    
    //Get list of Child Nodes
    List<MombasaCountyMenu> findByparentNodeId(String parentNodeId);
    
    MombasaCountyMenu findMombasaCountyMenuById(Long id);
    
}
