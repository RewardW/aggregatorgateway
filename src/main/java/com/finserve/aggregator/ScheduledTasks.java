/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.aggregator;

import com.finserve.aggregator.services.FinserveApiCall;
import com.finserve.aggregator.services.StrathmoreApiCall;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author Wambayi
 */
@Component
public class ScheduledTasks {
    
    static Log log = LogFactory.getLog(ScheduledTasks.class.getName());
    
    @PersistenceUnit
    private EntityManagerFactory emf;
    
    @Autowired
    StrathmoreApiCall strathApi;
    
    @Autowired
    FinserveApiCall finserveApi;
    
    
    @Scheduled(cron = "0 */1 * * * *")
    public void submitRegistrationData(){
        log.info("ScheduledTask: SubmitRegistrationData");
        
        EntityManager em = emf.createEntityManager();        
        String txnListQuery = "select user_id from registered_users where submitted = 0";
        List<BigInteger> obj = (List<BigInteger>)em.createNativeQuery(txnListQuery).getResultList();
        
        log.info("ScheduledTask: SubmitRegistrationData for "+obj.size()+" users");
        
        for(int x =0; x < obj.size(); x++){
            //log.info("ListItem:"+obj.get(x));
            strathApi.publishRegistrationInfo(obj.get(x).longValue());
        }
        
        em.close();
        /*
        for(Object[] object: obj){
            
        }
        */
    }
    
    @Scheduled(cron = "0 */2 * * * *")
    public void checkTxnStatus(){
        log.info("ScheduledTask: CheckTransactionStatus");
        
        EntityManager em = emf.createEntityManager();        
        String txnListQuery = "select transaction_id,payment_reference_id,status from payments"
                + " where status = 'pending'";
        List<Object[]> obj = (List<Object[]>)em.createNativeQuery(txnListQuery).getResultList();
        
        log.info("ScheduledTask: CheckTransactionStatus for "+obj.size()+" transactions");
        
        for(Object[] object: obj){
            finserveApi.getTxnStatus(Long.parseLong(object[0].toString()), object[1].toString());
        }
        
        em.close();
    }
}
