package com.finserve.aggregator;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.tomcat.jni.Time;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class AggregatorGatewayApplicationTests {

	public static void main( String args[]) {
            
            //String generatedId = generateId();
            
            //System.out.println("ID: "+ generatedId);
            
            readMFSREsponse();
	}
        
        public static String generateId(){
            String id = "";
            int min = 10001;
            int max = 99999;
            int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
            
            Calendar calendar = Calendar.getInstance();
            String idparam = Long.toString(calendar.getTimeInMillis());
            String firstPart = idparam.substring(4, idparam.length());
            
            id = 1+firstPart + randomNum;
            
            return id;
        }
        
        public static void readMFSREsponse(){
            try {
                String xmlResp = "<S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                        "    <S:Body>\n" +
                        "        <ns2:processingrequestingResponse xmlns:ns2=\"http://EquitySTKkenya.integration.mb.modefinserver.com/\">\n" +
                        "            <MFSPayments>\n" +
                        "                <TransactionRefNo>527077254251</TransactionRefNo>\n" +
                        "                <Status>Success</Status>\n" +
                        "                <ResultCode>000</ResultCode>\n" +
                        "            </MFSPayments>\n" +
                        "        </ns2:processingrequestingResponse>\n" +
                        "    </S:Body>\n" +
                        "</S:Envelope>";
                
                DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = domFactory.newDocumentBuilder();
                Document doc = builder.parse(new InputSource(new ByteArrayInputStream(xmlResp.getBytes("utf-8"))));
                XPath xPath = XPathFactory.newInstance().newXPath();
                
                NodeList nList = doc.getElementsByTagName("S:Envelope");
                Element eElement = (Element) nList.item(0);
                NodeList bodyList = eElement.getElementsByTagName("S:Body");
                Element bodyParams =  (Element) bodyList.item(0);
                NodeList genenericList = bodyParams.getElementsByTagName("ns2:processingrequestingResponse");
                Element genericParams =  (Element) genenericList.item(0);
                NodeList returnList = genericParams.getElementsByTagName("MFSPayments");
                Element returnParams =  (Element) returnList.item(0);
                String statusCode = returnParams.getElementsByTagName("Status").item(0).getTextContent();
                String responseCode = returnParams.getElementsByTagName("ResultCode").item(0).getTextContent();
                System.out.println("errorMessage:"+statusCode+"| Result"+responseCode);
                /*
                String status1 = "//S:Envelope/S:Body/ns2:processingrequestingResponse/MFSPayments/ResultCode";
                Node statusNode1 = (Node) xPath.compile(status1).evaluate(doc, XPathConstants.NODE);
                System.out.println("StatusNode: "+statusNode1.getTextContent());
                
                String status = "/S:Envelope/S:Body/ns2:processingrequestingResponse/MFSPayments/ResultCode";
                Node statusNode = (Node) xPath.compile(status).evaluate(doc, XPathConstants.NODE);
                System.out.println("StatusNode: "+statusNode.getTextContent());
                */
                
            } catch (ParserConfigurationException ex) {
                Logger.getLogger(AggregatorGatewayApplicationTests.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(AggregatorGatewayApplicationTests.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SAXException ex) {
                Logger.getLogger(AggregatorGatewayApplicationTests.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(AggregatorGatewayApplicationTests.class.getName()).log(Level.SEVERE, null, ex);
            } /*catch (XPathExpressionException ex) {
                Logger.getLogger(AggregatorGatewayApplicationTests.class.getName()).log(Level.SEVERE, null, ex);
            }*/
        }

}
